﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.application.model
{
    public class Huespedes
    {
        /** Atributos de la clase**/

        private String rutHuesped;
        private String nombreHuesped;
        private String apellidoHuesped;
        private int idHabitacion;
        private int numeroOrdenCompra;
        private String rutCliente;

        public Huespedes()
        {

        }

        public Huespedes(string rutHuesped, string nombreHuesped, string apellidoHuesped, int idHabitacion, int numeroOrdenCompra, string rutCliente)
        {
            this.RutHuesped = rutHuesped;
            this.NombreHuesped = nombreHuesped;
            this.ApellidoHuesped = apellidoHuesped;
            this.IdHabitacion = idHabitacion;
            this.NumeroOrdenCompra = numeroOrdenCompra;
            this.RutCliente = rutCliente;
        }

        public string RutHuesped { get => rutHuesped; set => rutHuesped = value; }
        public string NombreHuesped { get => nombreHuesped; set => nombreHuesped = value; }
        public string ApellidoHuesped { get => apellidoHuesped; set => apellidoHuesped = value; }
        public int IdHabitacion { get => idHabitacion; set => idHabitacion = value; }
        public int NumeroOrdenCompra { get => numeroOrdenCompra; set => numeroOrdenCompra = value; }
        public string RutCliente { get => rutCliente; set => rutCliente = value; }
    }
}

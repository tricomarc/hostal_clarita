﻿using Modelo.application.model;
using Persistencia.app.dao;
using Persistencia.DataSet1TableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Persistencia.DataSet1;

namespace Presentacion.application.controller
{
    /// <summary>
    /// Lógica de interacción para Módulo_Cliente.xaml
    /// </summary>
    public partial class Módulo_Cliente : UserControl
    {
        private ClienteDAO clienteDAO;
        public Módulo_Cliente()
        {
            clienteDAO = new ClienteDAO();
            InitializeComponent();
            
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            habilitaCampos(txt_Buscar.Text);

            dtg_Clientes.ItemsSource = clienteDAO.BuscarCliente(txt_Buscar.Text);
            dtg_Clientes.Columns[0].Visibility = Visibility.Hidden;
            dtg_Clientes.Columns[2].Visibility = Visibility.Hidden;
           

          

        }


       
        public void habilitaCampos(string texto)
        {
            if (!string.IsNullOrEmpty(txt_Buscar.Text))
            {
                dtg_Clientes.IsEnabled = true;
                btn_modificar.IsEnabled = true;
                btn_eliminar.IsEnabled = true;
            }
        }
    }
}

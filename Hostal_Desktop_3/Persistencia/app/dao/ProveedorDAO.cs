﻿using Modelo.application.model;
using Persistencia.DataSet1TableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Persistencia.DataSet1;

namespace Persistencia.app.dao
{
    public class ProveedorDAO
    {
        private PROVEEDORESTableAdapter adapter;

        public ProveedorDAO()
        {
            adapter = new PROVEEDORESTableAdapter();
        }


        //Método que lista todos los proveedotres -> será utilizado para cargar DataGrid dtg_proveedores
        public List<Proveedores> BuscaTodo()
        {
            List<Proveedores> proveedores = new List<Proveedores>();

            foreach (PROVEEDORESRow fila in adapter.GetData())
            {
                Proveedores proveedor = new Proveedores();
                proveedor.RutProveedor = fila.RUT_PROVEEDOR;
                proveedor.NombreProveedor = fila.NOMBRE_PROVEEDOR;
                proveedor.Direccion = fila.DIRECCION_PROVEEDOR;
                proveedor.RubroProveedor = fila.RUBRO_PROVEEDOR;
                proveedor.Telefono = (Int32)fila.TELEFONO;
                proveedor.CorreoElectronico = fila.CORREO_ELECTRONICO;
                proveedores.Add(proveedor);
            }

            return proveedores;
        }
    }
}
